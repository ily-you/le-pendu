<?php
/**
 * À l'aide de PHP, des sessions de la liste de mots, des images données et d'un peu de CSS
 * Construire un jeu du pendu.
 *
 * INITIALISATION :
 * -> L'ordinateur doit choisir un mot aléatoire au lancement
 * -> Afficher un texte à trou de la longueur du mot sélectionné
 * -> La première image doit s'afficher
 * -> Afficher le formulaire
 *
 * JEU :
 * -> Le joueur peut saisir une lettre (ni plus, ni moins)
 *      -> Si le joueur entre 0 ou plusieurs lettres (rien ne se passe ou message d'erreur)
 * -> Si la lettre sélectionnée n'est pas dans le mot, l'image suivante doit s'afficher
 * -> Si la lettre sélect. est dans le mot, elle doit s'afficher aux endroits où elle est présente dans le mot
 * -> Si toutes les lettres sont trouvées (condition de victoire) un message de réussite s'affiche
 * -> Si l'utilisateur s'est trompé 7 fois, la partie est perdue
 *      -> L'utilisateur ne peut plus saisir de lettre
 *      -> Un bouton 'Nouvelle partie' permet de relancer une partie
 */

session_start();

if (isset($_GET['newGame'])) {
    session_destroy();
    header('Location: index.php');
}

function motAleatoire()
{
    $dictionnaire = ['crayon', 'stylo', 'feutre', 'pointe', 'mine', 'gomme', 'dessin', 'coloriage', 'rayure',
        'peinture', 'pinceau', 'couleur', 'craie', 'papier', 'feuille', 'cahier', 'carnet', 'carton', 'ciseaux',
        'pliage', 'pli', 'colle', 'affaire', 'casier', 'caisse', 'trousse', 'cartable', 'jouet', 'jeu', 'pion',
        'domino', 'puzzle', 'cube', 'perle'];
    $nbMots = count($dictionnaire);
    return $dictionnaire[rand(0, $nbMots - 1)];
}

// initialisation du jeu
$afficherBoutonNouvellePartie = false;
if (!isset($_SESSION['mot'])) { // vrai quand la session est neuve (vide)
    $_SESSION['mot'] = motAleatoire(); // appelle la fonction motAleatoire() pour le stocker en session
    $_SESSION['texteTrouve'] = ''; // initialise la variable en chaine de caractères vide
    // cette boucle sert à construire le texte à trou ex : cahier -> ______
    foreach (str_split($_SESSION['mot']) as $letter) {
        $_SESSION['texteTrouve'] .= '_'; // $_SESSION['texteTrouve'] = $_SESSION['texteTrouve'] . '_';
    }
    $_SESSION['nbErrors'] = 0; // le nombre d'erreurs commence à 0
}

// récupération du formulaire
if (isset($_POST['lettre']) && strlen($_POST['lettre']) == 1) {
    $emplacements = [];
    // déterminer à quels endroits la lettre envoyée est dans le mot sélectionné
    foreach (str_split($_SESSION['mot']) as $index => $letter) {
        if ($letter == $_POST['lettre']) {
            // l'utilisateur a trouvé une lettre dans le mot, on sauvegarde l'emplacement
            $emplacements[] = $index;
        }
    }
    if (count($emplacements) == 0) { // aucun emplacement trouvé
        // la lettre n'a pas été trouvée dans le mot
        $_SESSION['nbErrors']++;
    } else {
        // la lettre est présente une ou plusieurs fois dans le mot
        foreach ($emplacements as $emplacement) { // pour chaque emplacement, remplacer les _ aux mêmes emplacements
            $_SESSION['texteTrouve'][$emplacement] = $_POST['lettre'];
        }
    }


} elseif (isset($_POST['lettre']) && strlen($_POST['lettre']) != 1) {
    // le champs 'lettre' a été envoyé avec un nombre de caractère différent de 1 (0 ou 2, 3, 4...)
    $message = 'Vous ne devez saisir qu\'une lettre';
}

if ($_SESSION['nbErrors'] >= 7) {
    // l'utilisateur a perdu
    $message = 'Vous avez perdu :(' ;
    $afficherBoutonNouvellePartie = true;
}
if ($_SESSION['mot'] == $_SESSION['texteTrouve']) {
    // l'utilisateur a trouvé le mot (victoire)
    $message = 'Vous avez gagné !';
    $afficherBoutonNouvellePartie = true;
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="style.css">
    <title>Jeu du pendu</title>
</head>
<body>
<img src="images/<?= ($_SESSION['nbErrors'] + 1) ?>.jpg" alt="">
<?= $_SESSION['texteTrouve'] ?>
<?php if (isset($message)) { echo "<p>$message</p>"; } ?>
<?php if ($afficherBoutonNouvellePartie === true) { ?>
    <a href="index.php?newGame">Nouvelle partie</a> <!-- $_GET['newGame'] -> valeur: "" -->
<?php } else { ?>
    <form action="" method="post">
        <input type="text" name="lettre">
        <input type="submit" value="Choisir">
    </form>
<?php } ?>
</body>
</html>